package br.com.k21;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;


public class TestCalculadoraComissao {

	@Test
	public void umaVendaDe1000ReaisRetornaComissaoDe50Reais() {
		int venda = 1000;
		int valorEsperado = 50;

		double retorno = CalculadoraComissao.calcular(venda);

		Assert.assertEquals(valorEsperado, retorno, 0);
	}

	@Test
	public void umaVendaDe100ReaisRetornaComissaoDe5Reais() {
		int venda = 100;
		int valorEsperado = 5;

		double retorno = CalculadoraComissao.calcular(venda);

		Assert.assertEquals(valorEsperado, retorno, 0);
	}


	@Test
	public void umaVendaDe10001ReaisRetornaComissaoDe600ReaisE6Centavos() {
		BigDecimal venda = new BigDecimal("10001");
		BigDecimal valorEsperado = new BigDecimal("600.06");

		BigDecimal retorno = CalculadoraComissao.calcular(venda);

		Assert.assertEquals(valorEsperado, retorno);
	}

	@Test
	public void umaVendaDe10000ReaisRetornaComissaoDe500Reais() {
		int venda = 10000;
		double valorEsperado = 500.00;

		double retorno = CalculadoraComissao.calcular(venda);

		Assert.assertEquals(valorEsperado, retorno, 0);
	}

	@Test
	public void umaVendaDe55ReaisE59CentavosRetornaComissaoDe2ReaisE77Centavos() {
		double venda = 55.59;
		double valorEsperado = 2.77;

		double retorno = CalculadoraComissao.calcular(venda);

		Assert.assertEquals(valorEsperado, retorno, 0);
	}	

}