package br.com.k21;

import java.math.BigDecimal;

public class CalculadoraComissao {

	static double FAIXA_SUPERIOR_COMISSAO = 0.06;
	static double FAIXA_INFERIOR_COMISSAO = 0.05;

	public static double calcular(double valorVenda) {
		double resultado = valorVenda * FAIXA_INFERIOR_COMISSAO;

		if(valorVenda > 10000) 
			resultado = valorVenda * FAIXA_SUPERIOR_COMISSAO;

		return Math.floor(resultado * 100) / 100; 
	}

	public static BigDecimal calcular(BigDecimal valorVenda) {

		if(valorVenda.compareTo(new BigDecimal("10000")) >= 0)
			return valorVenda.multiply(new BigDecimal("6")).divide(new BigDecimal("100"));
		return valorVenda.multiply(new BigDecimal("5")).divide(new BigDecimal("100"));
	}
}